package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpAttributeItem;

/**
 * 拓展属性值Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpAttributeItemMapper 
{
    /**
     * 查询拓展属性值
     * 
     * @param id 拓展属性值ID
     * @return 拓展属性值
     */
    public ErpAttributeItem selectErpAttributeItemById(String id);

    /**
     * 查询拓展属性值列表
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 拓展属性值集合
     */
    public List<ErpAttributeItem> selectErpAttributeItemList(ErpAttributeItem erpAttributeItem);

    /**
     * 新增拓展属性值
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 结果
     */
    public int insertErpAttributeItem(ErpAttributeItem erpAttributeItem);

    /**
     * 修改拓展属性值
     * 
     * @param erpAttributeItem 拓展属性值
     * @return 结果
     */
    public int updateErpAttributeItem(ErpAttributeItem erpAttributeItem);

    /**
     * 删除拓展属性值
     * 
     * @param id 拓展属性值ID
     * @return 结果
     */
    public int deleteErpAttributeItemById(String id);

    /**
     * 批量删除拓展属性值
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpAttributeItemByIds(String[] ids);
}
