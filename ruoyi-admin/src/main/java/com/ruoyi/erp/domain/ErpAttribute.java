package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 拓展属性对象 erp_attribute
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpAttribute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String attributeName;

    /** 属性key */
    @Excel(name = "属性key")
    private String attributeKey;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private String useState;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setAttributeName(String attributeName) 
    {
        this.attributeName = attributeName;
    }

    public String getAttributeName() 
    {
        return attributeName;
    }
    public void setAttributeKey(String attributeKey) 
    {
        this.attributeKey = attributeKey;
    }

    public String getAttributeKey() 
    {
        return attributeKey;
    }
    public void setUseState(String useState) 
    {
        this.useState = useState;
    }

    public String getUseState() 
    {
        return useState;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("attributeName", getAttributeName())
            .append("attributeKey", getAttributeKey())
            .append("useState", getUseState())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
