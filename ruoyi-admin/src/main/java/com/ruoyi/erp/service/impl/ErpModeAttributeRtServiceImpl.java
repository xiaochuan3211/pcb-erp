package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpModeAttributeRtMapper;
import com.ruoyi.erp.domain.ErpModeAttributeRt;
import com.ruoyi.erp.service.IErpModeAttributeRtService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产型号拓展属性关联Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpModeAttributeRtServiceImpl implements IErpModeAttributeRtService 
{
    @Autowired
    private ErpModeAttributeRtMapper erpModeAttributeRtMapper;

    /**
     * 查询生产型号拓展属性关联
     * 
     * @param id 生产型号拓展属性关联ID
     * @return 生产型号拓展属性关联
     */
    @Override
    public ErpModeAttributeRt selectErpModeAttributeRtById(String id)
    {
        return erpModeAttributeRtMapper.selectErpModeAttributeRtById(id);
    }

    /**
     * 查询生产型号拓展属性关联列表
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 生产型号拓展属性关联
     */
    @Override
    public List<ErpModeAttributeRt> selectErpModeAttributeRtList(ErpModeAttributeRt erpModeAttributeRt)
    {
        return erpModeAttributeRtMapper.selectErpModeAttributeRtList(erpModeAttributeRt);
    }

    /**
     * 新增生产型号拓展属性关联
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 结果
     */
    @Override
    public int insertErpModeAttributeRt(ErpModeAttributeRt erpModeAttributeRt)
    {
        erpModeAttributeRt.setId(IdUtils.fastSimpleUUID());
        erpModeAttributeRt.setCreateTime(DateUtils.getNowDate());
        return erpModeAttributeRtMapper.insertErpModeAttributeRt(erpModeAttributeRt);
    }

    /**
     * 修改生产型号拓展属性关联
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 结果
     */
    @Override
    public int updateErpModeAttributeRt(ErpModeAttributeRt erpModeAttributeRt)
    {
        erpModeAttributeRt.setUpdateTime(DateUtils.getNowDate());
        return erpModeAttributeRtMapper.updateErpModeAttributeRt(erpModeAttributeRt);
    }

    /**
     * 删除生产型号拓展属性关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpModeAttributeRtByIds(String ids)
    {
        return erpModeAttributeRtMapper.deleteErpModeAttributeRtByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除生产型号拓展属性关联信息
     * 
     * @param id 生产型号拓展属性关联ID
     * @return 结果
     */
    @Override
    public int deleteErpModeAttributeRtById(String id)
    {
        return erpModeAttributeRtMapper.deleteErpModeAttributeRtById(id);
    }
}
